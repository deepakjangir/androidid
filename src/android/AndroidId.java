package cordova.plugin.androidId;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import android.content.Context;
import android.provider.Settings;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class AndroidId extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("getAndroidId")) {
            this.getAndroidId(callbackContext);
            return true;
        }
        return false;
    }

    private void getAndroidId(CallbackContext callbackContext) {
        Context context = cordova.getActivity().getApplicationContext();
        String deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (deviceId != null && deviceId.length() > 0) {
            callbackContext.success(deviceId);
        } else {
            callbackContext.error("Something went wrong!");
        }
    }
}
