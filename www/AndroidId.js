var exec = require('cordova/exec');

exports.getAndroidId = function (arg0, success, error) {
    exec(success, error, 'AndroidId', 'getAndroidId', [arg0]);
};
